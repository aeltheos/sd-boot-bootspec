{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
with lib.types; {
  options.boot.loader.efi.efiSysMountPoints = mkOption {
    type = listOf str;
    default = [config.boot.loader.efi.efiSysMountPoint];
  };
  options.boot.loader.systemdBootBootspec.enable = mkEnableOption "systemdBootBootspec";
  options.boot.secureBoot = mkEnableOption "secureBoot";
  config.boot = mkIf config.boot.loader.systemdBootBootspec.enable {
    bootspec.enable = true;
    bootspec.extensions.org.crans = {
      # forwarding efi config
      efi = let cfg = config.boot.loader.efi; in {
        efiSysMountPoints = cfg.efiSysMountPoints;
        canTouchEfiVariables = cfg.canTouchEfiVariables;
      };
      # forwarding systemd-boot config
      systemd-boot = let cfg = config.boot.loader.systemd-boot; in {
        editor = cfg.editor;
        graceful = cfg.graceful;
        extraFiles = cfg.extraFiles;
        consoleMode = cfg.consoleMode;
        extraEntries = cfg.extraEntries;
        netbootxyz.enable = cfg.netbootxyz.enable;
        memtest86.enable = cfg.memtest86.enable;
        configurationLimit = cfg.configurationLimit;
        extraInstallCommands = cfg.extraInstallCommands;
      };
      # common loader options
      boot.loader.timeout = (builtins.toString config.boot.loader.timeout);
    };
    loader.external = {
      enable = true;
      installHook = let
        installer = pkgs.writeShellApplication {
          name="sd-boot_installer";
          runtimeInputs=with pkgs; [bash systemd coreutils jq rsync];
          text=(builtins.readFile ./installer.sh);
        };
      in "${installer}/bin/sd-boot_installer";
    };
  };
}
