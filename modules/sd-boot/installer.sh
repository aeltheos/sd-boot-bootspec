#!/bin/sh
# simple argument check
if [ -z "$1" ] || [ ! -d "$1" ]; then
  echo "Usage: $0 <config_dir>"
  exit 1
fi
# loading config
bootspec="$1/boot.json"
entries="$(find /nix/var/nix/profiles/ -name "system-*-link" | sort -n -t '-' -k 2)"

# common config
timeout="$(              jq '.v1.extensions.org.crans.boot.loader.timeout' "$bootspec")"

# efi config
efiSysMountPoints="$(    jq --raw-output '.v1.extensions.org.crans.efi.efiSysMountPoints[]' "$bootspec")"
#canTouchEfiVariables="$( jq '.v1.extensions.org.crans.efi.canTouchEfiVariables' "$bootspec")"

# systemd-boot config
editor="$(               jq '.v1.extensions.org.crans."systemd-boot".editor' "$bootspec")"
#gracefull="$(            jq '.v1.extensions.org.crans."systemd-boot".graceful' "$bootspec")"
#extraFiles="$(           jq '.v1.extensions.org.crans."systemd-boot".extraFiles' "$bootspec")"
consoleMode="$(          jq '.v1.extensions.org.crans."systemd-boot".consoleMode' "$bootspec")"
#extraEntries="$(         jq '.v1.extensions.org.crans."systemd-boot".extraEntries' "$bootspec")"
#configurationLimit="$(   jq '.v1.extensions.org.crans."systemd-boot".configurationLimit' "$bootspec")"
#extraInstallCommands="$( jq '.v1.extensions.org.crans."systemd-boot".extraInstallCommands' "$bootspec")"

# create a tmp dir to build and collect files.
tmp="$(mktemp -d -t 'systemd-boot-XXXXXXXXX')"
mkdir -p "$tmp/loader/entries"

# generate loader.conf
echo "# Generated file; DO NOT EDIT
title Nixos Bootloader
timeout $timeout
console-mode $consoleMode
editor $editor
auto-entries false
auto-firmware true
random-seed-mode with-system-token
default nixos-generation-*
" > "$tmp/loader/loader.conf"

# generation generation-${n} for each entries.
# TODO: flatten specialisations into entries.
mkdir -p "$tmp/EFI/nixos"
for entry in $entries
do
  # loading entry config
  init="$(         jq --raw-output '.v1.init' "$entry/boot.json")"
  initrd="$(       jq --raw-output '.v1.initrd' "$entry/boot.json")"
  kernel="$(       jq --raw-output '.v1.kernel' "$entry/boot.json")"
  kernelParams="$( jq --raw-output '.v1.kernelParams' "$entry/boot.json")"
  label="$(        jq --raw-output '.v1.label' "$entry/boot.json")"
  
  # symlinking kernel and initrd if they are not already symlinked.
  kernelName="$(echo "$kernel" | cut -d'/' -f4)"
  if [ ! -f "$tmp/EFI/nixos/$kernelName.efi" ]; then
    ln -s "$kernel" "$tmp/EFI/nixos/$kernelName.efi"
  fi

  initrdName="$(echo "$initrd" | cut -d'/' -f4)"
  if [ ! -f "$tmp/EFI/nixos/$initrdName.efi" ]; then 
    ln -s "$initrd" "$tmp/EFI/nixos/$initrdName.efi"
  fi

  # generating nixos-generation-${n}.conf
  gen_number="$(echo "$entry" | cut -d'-' -f2)"
  echo "
  title NixOS
  version Generation $gen_number $label
  linux /EFI/nixos/$kernelName.efi
  initrd /EFI/nixos/$initrdName.efi
  options init=$init $kernelParams
  " > "$tmp/loader/entries/nixos-generation-$gen_number.conf"
done
# installing systemd.
for esp in $efiSysMountPoints; do
  echo "Installing sd-boot on $esp" 
  # os-release info for nixos
  if [ ! -f "/etc/os-release" ]; then
    cp "$1/etc/os-release" /etc/os-release
  fi
  # install sd-boot
  bootctl "--esp-path=$esp" install
  if [ ! -d "$esp/EFI/nixos" ]; then
    mkdir -p "$esp/EFI/nixos"
  fi
  # apply config
  rsync -rL --delete "$tmp/EFI/nixos/" "$esp/EFI/nixos/"
  rsync -rL --delete "$tmp/loader/entries/" "$esp/loader/entries/"
  cp "$tmp/loader/loader.conf" "$esp/loader/loader.conf"
  sync
  echo "Done installing on $esp"
done
# cleaning up artefacts
rm -rf "$tmp"
echo "done installing systemd-boot"