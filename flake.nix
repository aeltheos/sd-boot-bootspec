{
  description = "Try at implementing secure boot with systemd for bootspec.";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "nixpkgs/nixos-unstable";

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachSystem (["x86_64-linux"]) (
      system: let
        pkgs = nixpkgs.legacyPackages.${system};
      in rec {
        formatter = pkgs.alejandra;
        devShell = pkgs.mkShell {
          buildInputs = [
            pkgs.sbctl
          ];
        };
        nixosModules.sd-boot = ./modules/sd-boot/sd-boot.nix;
        checks.sd-boot = pkgs.nixosTest {
          name = "sd-boot test";
          nodes = {
            machine = {config, pkgs,...}:{
              imports = [nixosModules.sd-boot];
              virtualisation.useBootLoader = true;
              virtualisation.memorySize = 4096;
              virtualisation.useEFIBoot = true;
              boot.loader.systemdBootBootspec.enable = true;
              boot.loader.efi.canTouchEfiVariables = true;
            };
          };
          testScript = ''
            machine.start(allow_reboot = True)
            machine.wait_for_unit("multi-user.target")
          '';

        };
      }

    );
}
