# sd-boot-bootspec

An attempt at making an easy to maintain sd-boot installer. Using bootspec to reduce the complexity and enabling new feature such as secure boot.

Current priorities would be:
 - better structuration and cleaning up syntax, adding small QOL option.
 - Then; once this is done, starting to work on specialization, generation limit, and secure boot.


 ## Specialization
 THe cleanest and easiest way might be to flatten the specialization as if they where standard entries.

## Generation Limit
Probably just need to truncate the entry list.

## Secure boot
Clearly the hardest and most ambitious task. Would sign $tmp/EFI/nixos and sd-boot and place signatures accordingly, enroll key, and set secure boot.
Will need an external audit.